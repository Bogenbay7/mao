//
//  LocationCell.swift
//  Map
//
//  Created by Ayazhan on 03.02.2022.
//

import UIKit

class LocationCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    static let identifier = "LocationCell"
    static let nib = UINib(nibName: "LocationCell", bundle: nil)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
