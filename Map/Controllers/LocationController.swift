//
//  LocationController.swift
//  Map
//
//  Created by Ayazhan on 03.02.2022.
//

import UIKit

class LocationController: UIViewController {

    var point2: [Place]?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(LocationCell.nib, forCellReuseIdentifier: LocationCell.identifier)
        // Do any additional setup after loading the view.
    }


}
extension LocationController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return point2!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LocationCell.identifier) as! LocationCell
        cell.locationLabel.text = "\(point2![indexPath.row].long)  | \(point2![indexPath.row].lati)"
        cell.titleLabel.text = "\(point2![indexPath.row].title ?? "No")"
        print("\(point2![indexPath.row].title ?? "Nope")")
        return cell
    }
    
    
}
