//
//  ViewController.swift
//  Map
//
//  Created by Ayazhan on 03.02.2022.
//

import UIKit
import MapKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {

    let locationManager = CLLocationManager()
    var places: [Place]? = []
 
  
    
    @IBOutlet weak var change: UISegmentedControl!
    @IBOutlet weak var mapView: MKMapView!


    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func addPin(){
        for i in 0 ..< places!.count{
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: places![i].lati, longitude: places![i].long)
            annotation.title = "\(places![i].title ?? "Astana")"
            mapView.addAnnotation(annotation)
           // alert()
            }
        
    }

    @IBAction func folderPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "folder") as! LocationController
        vc.point2 = places
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func changeSegment(_ sender: Any) {
        switch change.selectedSegmentIndex
         {
         case 0:
            mapView.mapType = .standard
         case 1:
            mapView.mapType = .satellite
        case 2:
            mapView.mapType = .hybrid
         default:
             break
         }
    }
  
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            // alert
            let alertController = UIAlertController(title: "Name it", message: "", preferredStyle: .alert)
            alertController.addTextField { textField in
                textField.placeholder = "Name"
                textField.isSecureTextEntry = false
                textField.textAlignment = .center
            }
            // touch
            let touchPoint = touch.location(in: mapView)
            let location = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                print("Canelled")
            }
            let confirmAction = UIAlertAction(title: "Submit", style: .default) { [self] _ in
                places!.append(Place.init(title: "\(alertController.textFields?.first!.text ?? "nope")", lati: location.latitude, long:location.longitude))
                addPin()
            }
         
            print ("\(location.latitude), \(location.longitude)")
            print("\(alertController.textFields?.first!.text)")
            print ("\(places?.count ?? 1)")
         
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            present(alertController, animated: true, completion: nil)
        }
    }
}



